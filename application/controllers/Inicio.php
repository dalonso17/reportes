<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Inicio
 * @property CI_Loader $load 
 * @property MReportes $reporte 
 * @author David A
 */
class Inicio extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('MReportes', 'reporte');
    }

    public function index() {

        $lista = array(
            'Reporte ALL' => base_url('Inicio/crudosAll'),
        );

        $data = array(
            'reportesLst' => $lista
        );

        $this->load->view('lista_reportes', $data);
    }

    public function crudosAll() {

        $reg = $this->reporte->get_traficosAll();
        $this->reporte->arma_excel($reg);
        
    }

}
