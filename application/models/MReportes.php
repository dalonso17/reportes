<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MReportes
 * @property CI_Loader $load 
 * @property CI_DB $db
 * @author David A
 */
class MReportes extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }
    
    public function get_traficosAll() {
                
        $this->db->from('vst_colas_all');
        $this->db->limit(5);
        
        $query = $this->db->get();
        
        return $query->result_object();
        
    }
    
    public function arma_excel($query, $titulos = NULL, $opciones = NULL, $fileName = NULL) {
        
        $this->load->library('Excel');
        // Autor: David Alonso 2016.
        // Se crea el encabezado del excel. abecedario de 65 a 90        
        if($query) {
            $columnas = range('A', 'Z');
            foreach ($columnas as $key => $value) {
                $columnas[] = $columnas[0] . $value;
            }

            $this->excel->setActiveSheetIndex(0);

            $rowInicio = 1;
            if ($opciones) {

            }

            //        Arma el Titulo
            if ($titulos) { // Si recibe el titulo.
                foreach ($titulos as $key => $value) {
                    $this->excel->getActiveSheet()->setCellValue($columnas[$key] . $rowInicio, $value);
                    $ultima_col = $columnas[$key];
                    $col = $key;
                }
            } else { // si  no recibe.
                $col = 0;
                foreach ($query[0] as $indice => $valor) {
                    $this->excel->getActiveSheet()->setCellValue($columnas[$col] . $rowInicio, $indice);
                    $ultima_col = $columnas[$col];
                    $col++;
                }
            }

    //        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit('D'.$i, $fila->nro_doc, PHPExcel_Cell_DataType::TYPE_STRING);
    //        Fin Titulo
    //        Cuerpo del Excel
            $i = $rowInicio + 1;
            foreach ($query as $key => $value) {
                $subI = 0;
                foreach ($value as $subIndice => $dato) {
                    if (is_numeric(trim($dato))) {
                        $this->excel->getActiveSheet()->setCellValueExplicit($columnas[$subI] . $i, trim($dato), ($subIndice == 'precio') ? PHPExcel_Cell_DataType::TYPE_NUMERIC : PHPExcel_Cell_DataType::TYPE_STRING);
    //                    $this->excel->getActiveSheet()->getStyle($columnas[$subI] . $i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    } else {
                        $this->excel->getActiveSheet()->setCellValue($columnas[$subI] . $i, trim($dato));
                    }
                    $subI++;
                }
                $i++;
            }

    //        Fin Cuerpo Excel

            for ($v = 0; $v <= $col; $v++) {
                $this->excel->getActiveSheet()->getColumnDimension("$columnas[$v]")->setAutoSize(true);
            }

            if (!$opciones) {
                $this->excel->getActiveSheet()->setSharedStyle(
                        $this->colorGris(), "A" . $rowInicio . ":" . $ultima_col . $rowInicio
                );
            }

            $hoy = date("YmdHis");
            $fileName = "reporte_" . ($fileName ? $fileName : '') . $hoy . ".xlsx";

            ob_clean();
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $fileName . '"');
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
            $objWriter->save('php://output');

        }
    }
    
    private function colorGris() {
        return (new PHPExcel_Style())
                        ->applyFromArray([
                            'fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => '9E9E9E')
                            ),
                            'font' => array(
                                'color' => array('rgb' => 'FFFFFF'),
                                'name' => 'Calibri',
                                'size' => '11'
                            ),
        ]);
    }
    

}
